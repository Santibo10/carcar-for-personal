import { useState } from "react";

const ModelForm = ({manufacturers, GetModelsData}) => {

  const [success, setSuccess] = useState(false)
  const [name, setName] = useState("")
  const [picture, setPicture] = useState("")
  const [manufacturer, setManufacturer] = useState("")

  const handleChange = (event, callBack) => {
      const value = event.target.value;
      callBack(value)
  }

  const setToFalse = (callback) => {
    callback(false)
  }

  const handleMessages = (callBack) => {
    setTimeout(() => (setToFalse(callBack)), 10000)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.picture_url = picture;
    data.manufacturer_id = manufacturer;

    const modelsUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }
    const response = await fetch(modelsUrl, fetchConfig);
    if (response.ok) {

      GetModelsData()
      setName("")
      setPicture("")
      setManufacturer("")
      setSuccess(true)
      handleMessages(setSuccess)
    }
  }


  return (
    <>
    {success && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-success p-2" role="alert">A new Automobile Model has been added!</div></div>}
    <div className="row justify-content-center mt-5">
      <div className="col-6 card">
        <div className="card-body">
        <form onSubmit={handleSubmit} className="row g-3">
            <h1>Create Vehicle Model</h1>
              <input required value={name} onChange={(event) => handleChange(event, setName)} type="text" className="form-control" id="modelName" placeholder="Model Name"/>
              <input required value={picture} onChange={(event) => handleChange(event, setPicture)} type="text" className="form-control" id="modelPicture" placeholder="Picture Url"/>
              <select required value={manufacturer} onChange={(event) => handleChange(event, setManufacturer)} className="form-select mb-3" aria-label=".form-select-lg example">
                <option value="">Manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return <option value={manufacturer.id} key={manufacturer.id}>{manufacturer.name}</option>
                })}

              </select>
              <button type="submit" className="w-25 btn btn-primary mb-3">Create</button>
          </form>
        </div>
      </div>
    </div>
    </>
  )
}
export default ModelForm;
