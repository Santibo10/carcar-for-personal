const ModelList = ({models}) => {
  return (
    <>
    <h3 className="mt-5">Models</h3>
     <table className="table table-striped table-hover">
      <thead className="table border-dark">
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(model => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td><img style={{width:"300px"}}src={model.picture_url}></img></td>
              </tr>
            )
        })}
      </tbody>
     </table>
    </>
  )
}
export default ModelList
