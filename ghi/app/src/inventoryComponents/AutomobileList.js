function AutomobileList({automobiles}){
    if (automobiles === undefined) {
        return null;
    }
    return (
        <>
            <table className="table table-bordered table-striped table-hover mt-5 text-center">
                <thead>
                    <tr>
                        <th style={{width: "300px"}}>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th style={{width: "200px"}}>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => {
                        return (
                            <tr key={automobile.href}>
                                <td>{ automobile.vin }</td>
                                <td>{ automobile.color }</td>
                                <td>{ automobile.year }</td>
                                <td>{ automobile.model.name }</td>
                                <td>{ automobile.model.manufacturer.name }</td>
                                <td>{ automobile.sold.toString() }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default AutomobileList