import { useState } from "react";

function AutomobileForm({models, GetAutomobilesData}){
    const [color, setColor] = useState("")
    const [year, setYear] = useState("")
    const [vin, setVin] = useState("")
    const [model, setModel] = useState("")

    const changeState = ({ target }, cb) => {
        const { value } = target;
        cb(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model

        const autoUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();


            GetAutomobilesData();
            setColor('');
            setYear('');
            setVin('');
            setModel('');
            event.target.reset();
        }
    }



    return (
        <>
        <div className="row">
            <div className="offset-3 col-6 row-10">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Create a Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="row">
                            <select value={model} onChange={(event) => changeState(event, setModel)} className="form-select mb-3" aria-label=".form-select-lg example">
                                <option value="">Choose a Model</option>
                                {models.map(model =>{
                                    return(
                                        <option value={model.id} key={model.id}>{model.name}</option>
                                    );
                                })}
                            </select>
                            <input required type="text" name="Color" placeholder="Color" className="form-control mb-3" onChange={(event) => changeState(event, setColor)} />
                            <input placeholder="Year" type="number" name="Year" className="form-control mb-3" onChange={(event) => changeState(event, setYear)} />
                            <input placeholder="VIN" type="text" name="VIN" className="form-control mb-3" onChange={(event) => changeState(event, setVin)} />
                            <button type="submit" className="w-25 btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default AutomobileForm