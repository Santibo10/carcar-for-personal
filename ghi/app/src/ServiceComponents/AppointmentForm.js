import { useState } from "react";

function AppointmentForm({technicians, GetAppointmentData}){
    const [dateTime, setDateTime] = useState("");
    const [reason, setReason] = useState("");
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');

    const changeState = ({ target }, cb) => {
        const { value } = target;
        cb(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        const date = new Date(dateTime)
        data.date_time = date.toISOString();
        data.reason = reason;
        data.vin = vin
        data.customer = customer
        data.technician = technician

        const aptUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(aptUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            GetAppointmentData();
            setDateTime('');
            setReason('');
            setVin('')
            setCustomer('')
            setTechnician('')
            event.target.reset();
        }
    };
    return (
        <>
        <div className="row">
            <div className="offset-3 col-6 row-10">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Schedule an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="row">
                            <select value={technician} required onChange={(event) => changeState(event, setTechnician)} className="form-select mb-3" aria-label=".form-select-lg example">
                                <option>Choose a Technician</option>
                                {technicians.map(technician =>{
                                    return(
                                        <option value={technician.employee_id} key={technician.id}>{technician.first_name} {technician.last_name}</option>
                                    );
                                })}
                            </select>
                            <input required type="datetime-local" name="date time" className="form-control mb-3" onChange={(event) => changeState(event, setDateTime)} />
                            <input placeholder="Reason" type="text" name="reason" className="form-control mb-3" onChange={(event) => changeState(event, setReason)} />
                            <input placeholder="VIN" type="text" name="VIN" className="form-control mb-3" onChange={(event) => changeState(event, setVin)} />
                            <input placeholder="Your Name" type="text" name="customer" className="form-control mb-3" onChange={(event) => changeState(event, setCustomer)} />
                            <button type="submit" className="w-25 btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    );
}

export default AppointmentForm