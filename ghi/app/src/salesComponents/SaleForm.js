import { useState } from "react";
import Spinner from "./Spinner";



const SaleForm = ({salespeople, customers, unsoldAutomobiles, GetSalesData, GetAutomobilesData}) => {

  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const [vin, setVin] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");

  const handleChange = (event, callBack) => {
    const value = event.target.value;
    callBack(value);
  }

  const updateAutomobileStatus = async (vinNum) => {
    const automobileUrl = `http://localhost:8100/api/automobiles/${vinNum}/`;
    const data = {sold: true};
    const FetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
  }
  const response = await fetch(automobileUrl, FetchConfig);
  if (response.ok) {
    GetAutomobilesData()
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    setLoading(true)

    const data = {}
    data.automobile = vin;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const salesUrl = "http://localhost:8090/api/sales/";
    const FetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }

    const response = await fetch(salesUrl, FetchConfig);
    if (response.ok) {
      updateAutomobileStatus(vin)
      GetSalesData()
      setVin("")
      setSalesperson("")
      setCustomer("")
      setPrice("")
      setLoading(false)
      setSuccess(true)
      timer()
      if (error === true) {
        setError(false)
      }

    } else {
      setLoading(false)
      setError(true)
    }
  }
  const hideMessage = () => {
    setSuccess(false)
  }

  const timer = () => {
    setTimeout(hideMessage, 10000)
  }

  return (
    <>
    {success && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-success p-2" role="alert">A new Sale has been added!</div></div>}
    {error && <div className="row justify-content-center"><div className="position-absolute col-8 alert alert-danger p-2" role="alert">VIN not yet registered. Please give our servers a minute and try again.</div></div>}
    <div className="row justify-content-center mt-5">
      <div className="col-6 card">
        <div className="card-body">
        <form onSubmit={handleSubmit} className="row g-3">
            <h1>Record a new Sale</h1>
              <label className="form-label mb-0" htmlFor="selectVin">Automobile VIN</label>
              <select required value={vin} onChange={(event) => handleChange(event, setVin)} className="form-select mb-3 mt-0" aria-label=".form-select-lg example" id="selectVin">
                  <option value="">Choose a VIN</option>
                  {unsoldAutomobiles.map(automobile => {
                    return <option value={automobile.vin} key={automobile.id}>{automobile.vin}</option>
                  })}
              </select>
              <label className="form-label mb-0" htmlFor="selectSalesperson">Salesperson</label>
              <select required value={salesperson} onChange={(event) => handleChange(event, setSalesperson)} className="form-select mb-3 mt-0" aria-label=".form-select-lg example" id="selectSalesperson">
                  <option value="">Choose a salesperson</option>
                  {salespeople.map(salesperson => {
                    return <option value={salesperson.employee_id} key={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                  })}
              </select>
              <label className="form-label mb-0" htmlFor="selectCustomer">Customer</label>
              <select required value={customer} onChange={(event) => handleChange(event, setCustomer)} className="form-select mb-3 mt-0" aria-label=".form-select-lg example" id="selectCustomer">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                    return <option value={customer.id} key={customer.id}>{customer.first_name} {customer.last_name}</option>
                  })}
              </select>
              <label className="form-label mb-0 text-muted" htmlFor="Price">ex. 50000.00</label>
              <input required value={price} onChange={(event) => handleChange(event, setPrice)} type="number" className="form-control mt-0" id="Price" placeholder="Price in USD"/>
              {loading ? <Spinner />:<button type="submit" className="w-25 btn btn-primary mb-3">Create</button>}
          </form>
        </div>
      </div>
    </div>
    </>
  )
}
export default SaleForm;
