import { useState } from "react";

function ServiceHistory({appointments}) {
    const [vin, setVin] = useState("");
    const [inpValue, setValue] = useState("");

    const handleValue = (event) => {
        const value = event.target.value;
        setValue(value);
    }

    const handleSearch = (event) => {
        event.preventDefault();
        setVin(inpValue);
    }

    if (appointments === undefined) {
        return null;
    }

    const filteredAppointments = appointments.filter(
        (appointment) => {
            return appointment.status !== "" && appointment.vin.toLowerCase().includes(vin.toLowerCase());
        }
    );

    return (
        <>
            <div className="row">
                <h1 className="text-center">Service History</h1>
                <form onSubmit={handleSearch} className="col-12 d-flex justify-content-center">
                    <div className="col-md-6 col-lg-4">
                        <div className="shadow-p4 mt-4">
                            <input required onChange={handleValue} value={inpValue} type="search" name="search" placeholder="Enter your VIN to search" className="form-control" aria-label="Search" />
                        </div>
                    </div>
                    <div className="d-flex align-items-end">
                        <div className="shadow-p4 mt-4">
                            <button className="btn btn-outline-success ms-3" type="">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <table className="table table-bordered table-striped table-hover mt-5 text-center">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map(appointment => {
                        const userTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                        const date_time = appointment.date_time
                        const date = new Date(date_time);
                        const options = {
                            month: "numeric",
                            day: "numeric",
                            year: "numeric",
                            hour: "numeric",
                            minute: "numeric",
                            hour12: true,
                            timeZone: userTimezone,
                        };
                        const formattedDate = new Intl.DateTimeFormat('en-US', options).format(date);
                        return(
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ServiceHistory;