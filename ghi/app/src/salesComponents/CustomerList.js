const CustomerList = ({customers}) => {
  return (
    <>
    <h3 className="mt-5">Customers</h3>
     <table className="table table-striped table-hover">
      <thead className="table border-dark">
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Phone Number</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        {customers.map(customer => {
            return (
              <tr key={customer.id}>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>({customer.phone_number.slice(0,3)})-{customer.phone_number.slice(3,6)}-{customer.phone_number.slice(6)}</td>
                <td>{customer.address}</td>
              </tr>
            )
        })}
      </tbody>
     </table>
    </>
  )
}
export default CustomerList;
