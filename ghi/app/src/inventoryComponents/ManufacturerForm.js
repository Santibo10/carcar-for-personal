import { useState } from "react";

const ManufacturerForm = ({GetManufacturersData, manufacturers}) => {

  const [success, setSuccess] = useState(false)
  const [fail, setFail] = useState(false)
  const [name, setName] = useState("");

  const handleChange = (event) => {
    const value = event.target.value;
    setName(value)
  }

  const setToFalse = (callback) => {
    callback(false)
  }

  const handleMessages = (callBack) => {
    setTimeout(() => (setToFalse(callBack)), 10000)
  }

  const checkUniqueId = () => {
    for (let i of manufacturers) {
      if (i.name === name) {
        return false;
      }
    }
    return true;
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const check = checkUniqueId()
    if ( check === false) {
      setSuccess(false)
      setFail(true)
      handleMessages(setFail)
      return false;
    }
    const data = {}
    data.name = name;

    const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }
    const response = await fetch(manufacturersUrl, fetchConfig);
    if (response.ok) {

      GetManufacturersData()
      setName("")
      setFail(false)
      setSuccess(true)
      handleMessages(setSuccess)

    }
  }

  return (
    <>
    {fail && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-danger p-2" role="alert">The Manufacturer already exists</div></div>}
    {success && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-success p-2" role="alert">A new Manufacturer has been added!</div></div>}
    <div className="row justify-content-center mt-5">
      <div className="col-6 card">
        <div className="card-body">
        <form onSubmit={handleSubmit} className="row g-3">
            <h1>Create Manufacturer</h1>
              <input required value={name} onChange={handleChange} type="text" className="form-control" id="ManufacturerName" placeholder="Manufacturer"/>
              <button type="submit" className="w-25 btn btn-primary mb-3">Create</button>
          </form>
        </div>
      </div>
    </div>
    </>
  )


}
export default ManufacturerForm;
