import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className='nav-link text-white' aria-current="page" to='/'>Home</NavLink>
            </li>
            <li className='nav-item dropdown'>
              <NavLink className='nav-link dropdown-toggle text-white' to="#" role='button' data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className='dropdown-menu'>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/inventory/manufacturers/list">Manufacturers List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/inventory/manufacturers/new">Add a Manufacturer</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/inventory/models/list">Models List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/inventory/models/new">Add a Car Model</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/inventory/automobiles/list">Automobiles List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/inventory/automobiles/new">Add a Automobile</NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink className='nav-link dropdown-toggle text-white' to="#" role='button' data-bs-toggle="dropdown" aria-expanded="false">
                Automobile-Service
              </NavLink>
              <ul className='dropdown-menu'>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-service/technicians/list">Technicians List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/automobile-service/technicians/new">Add a Technician</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-service/appointments/list">List of Appointments</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/automobile-service/appointments/new">Schedule an Appointment</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-service/history">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink className='nav-link dropdown-toggle text-white' to="#" role='button' data-bs-toggle="dropdown" aria-expanded="false">
                Automobile-Sales
              </NavLink>
              <ul className='dropdown-menu'>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/salespeople/list">Salespeople List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/salespeople/new">Add a Salesperson</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/customers/list">Customers List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/customers/new">Add a Customer</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/sales/list">Sales List</NavLink>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/sales/new">Add a Sale</NavLink>
                </li>
                <li><hr className='dropdown-divider' /></li>
                <li>
                  <NavLink className='dropdown-item text-black'  to="/automobile-sales/history">Salesperson History</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
