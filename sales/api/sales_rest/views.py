from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import Salesperson, Customer, Sale, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id"]


class SalespeopleEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespeopleEncoder(),
        "customer": CustomerEncoder(),
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople}, encoder=SalespeopleEncoder, safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalespeopleEncoder, safe=False)
        except:
            return JsonResponse({"message": "could not create Salesperson"}, status=400)


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, id):
    if request.method == "GET":
        try:
            person = Salesperson.objects.get(employee_id=id)
            return JsonResponse(
                person,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "invalid request"}, status=400)
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=id)
            salesperson.delete()
            return JsonResponse(salesperson, encoder=SalespeopleEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "invalid request"}, status=404)


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers}, encoder=CustomerEncoder, safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(customers, encoder=CustomerEncoder, safe=False)
        except:
            return JsonResponse({"message": "could not create Customer"}, status=400)


@require_http_methods(["GET", "DELETE"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "invadil customer id"}, status=404)
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "invalid request"}, status=404)


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"messgae": "invalid Automobile vin"}, status=404)
        except Salesperson.DoesNotExist:
            return JsonResponse({"messgae": "invalid Salesperson id"}, status=404)
        except Customer.DoesNotExist:
            return JsonResponse({"messgae": "invalid Costumer id"}, status=404)

        content["automobile"] = automobile
        content["salesperson"] = salesperson
        content["customer"] = customer
        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Invalid Sale id"}, status=404)
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "invalid Sale id"}, status=404)
