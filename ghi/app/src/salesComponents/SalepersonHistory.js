import { useState } from "react";

const SalespersonHistory = ({salespeople, sales}) => {

  const [person, setPerson] = useState({})
  const [salesList, setSalesList] =useState([])

  const handleChange = (event) => {
    const value = event.target.value;
    setPerson(value);
    const filtered = sales.filter(sale => sale.salesperson.employee_id === value)
    setSalesList(filtered)
  }

  return (
    <>
    <h1 className="mt-5">Saleperson History</h1>
      <select value={person} onChange={(event) => handleChange(event, setPerson)} className="form-select mb-3 mt-0" aria-label=".form-select-lg example">
          <option value="">Choose a salesperson</option>
          {salespeople.map(person => {
            return <option value={person.employee_id} key={person.id}>{person.first_name} {person.last_name}</option>
          })}
      </select>
     <table className="table table-striped">
      <thead className="table border-dark">
        <tr>
          <th>Salesperson Employee ID</th>
          <th>Salesperson Name</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
      {salesList.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            )
        })}
      </tbody>
     </table>
    </>
  )
}
export default SalespersonHistory;
