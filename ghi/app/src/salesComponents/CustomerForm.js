import { useState } from "react";
import Spinner from "./Spinner";

const CustomerForm = ({GetCustomersData}) => {

  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleChange = (event, callBack) => {
    const value = event.target.value;
    callBack(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    setLoading(true)
    const data={};
    data.first_name = first[0].toUpperCase()+first.slice(1).toLowerCase();
    data.last_name = last[0].toUpperCase()+last.slice(1).toLowerCase();
    data.address = address;
    data.phone_number = phoneNumber;

    const custonerUrl = "http://localhost:8090/api/customers/";
    const FetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }
    const response = await fetch(custonerUrl, FetchConfig);
    if (response.ok) {
      GetCustomersData()

      setFirst("")
      setLast("")
      setAddress("")
      setPhoneNumber("")
      setLoading(false)
      setSuccess(true)
      timer()
    }
  }

  const hideMessage = () => {
    setSuccess(false)
  }

  const timer = () => {
    setTimeout(hideMessage, 10000)
  }

  return (
    <>
    {success && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-success p-2" role="alert">A new Customer has been added!</div></div>}
    <div className="row justify-content-center mt-5">
      <div className="col-6 card">
        <div className="card-body">
        <form onSubmit={handleSubmit} className="row g-3">
            <h1>Add a Customer</h1>
              <input required value={first} onChange={(event) => handleChange(event, setFirst)} type="text" className="form-control" id="FirstName" placeholder="First Name"/>
              <input required value={last} onChange={(event) => handleChange(event, setLast)} type="text" className="form-control" id="LastName" placeholder="Last Name"/>
              <input required value={address} onChange={(event) => handleChange(event, setAddress)} type="text" className="form-control" id="Address" placeholder="Address"/>
              <label className="form-label mb-0 text-muted" htmlFor="PhoneNumber">ex. 3036784431 (10 digits)</label>
              <input required value={phoneNumber} onChange={(event) => handleChange(event, setPhoneNumber)} type="tel" pattern="[0-9]{10}" className="form-control mt-0" id="PhoneNumber" placeholder="US phone number"/>
              {loading ? <Spinner />:<button type="submit" className="w-25 btn btn-primary mb-3">Create</button>}
          </form>
        </div>
      </div>
    </div>
    </>
  )
}
export default CustomerForm;
