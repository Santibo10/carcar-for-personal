const ManufacturerList = ({manufacturers}) => {
  return (
    <>
    <h3 className="mt-5">Manufacturers</h3>
     <table className="table table-striped table-hover">
      <thead className="table border-dark">
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            )
        })}
      </tbody>
     </table>
    </>
  )
}
export default ManufacturerList;
