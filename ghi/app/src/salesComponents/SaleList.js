
const SaleList = ({sales}) => {
  if (sales.length < 1) {
    return (
      <h3 className="mt-5">You Don't Have Any Sales Yet</h3>
    )
  } else {
    return (
      <>
      <h3 className="mt-5">Sales</h3>
       <table className="table table-striped table-hover">
        <thead className="table border-dark">
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
              return (
                <tr key={sale.id}>
                  <td>{sale.salesperson.employee_id}</td>
                  <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.price}</td>
                </tr>
              )
          })}
        </tbody>
       </table>
      </>
    )
  }
}
export default SaleList;
